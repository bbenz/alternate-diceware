dicewareList = open('diceware.wordlist.txt')
custom = open('custom.txt', 'w')
leftHand = 'qwertasdfgzxcvb'
rightHand = 'yuiophjklnm'

for row in dicewareList:
    word = (row.split())[1];
    length = len(word)
    i = 0
    alternate = True
    if length < 3:
        alternate = False
    while alternate:
        # Break when we exceed the length of the string
        if i+1 == length:
            break
        # if it has characters not in our hands set false
        if (word[i] not in leftHand) and (word[i] not in rightHand):
            alternate = False
        if (word[i] in leftHand) and (word[i+1] in leftHand):
            alternate = False
        if (word[i] in rightHand) and (word[i+1] in rightHand):
            alternate = False
        i = i + 1
    print(word, length, alternate)
    if alternate:
        word = word + '\n'
        custom.write(word)
