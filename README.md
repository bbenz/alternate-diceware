# Alternating Hands Diceware List

Create a passphrase that allows you to type using only words that that alternate hands on a qwerty keyboard

## Smooth typing - target audience

This little project might be interesting to you if you fit into any of the following categories:

* You're overly concerned with keyboard ergonomics
* You enjoy mnemonics
* You play a percussion instrument
* You hate the qwerty keyboard but can't/don't want to change to a different layout
* You use Vim, and remembering awkward keyboard combinations is fun for you

## Background

Once I had a long password that I seemed to never type correctly on the first try.  Upon examination, I realized the string "erc" was awkward to type because my middle and index finger would collide while touch typing quickly.  I changed my password to something that was more comfortable to type.

## How to use

Use this like a regular dicewarelist.  If you have a couple of d20 laying around (let's be honest, if you're interested in this project you have a couple of d20 laying around) you can roll three of them to generate a single word.  Do this at least 5 times for a strong password.  For you muggles out there, go grab regular six sided dice from the copy of monopoly you have gathering dust in a closet upstairs.  Roll 5 of them to generate a single word, repeat 5x to generat a passphrase.

After you've rolled for 5 (or more) words, create a strong menmonic for these 5 (or more) words.



### Creation process

English words were taken from the frequency list found on Peter Norvig's website.
I used awk to strip the frequency count out of the list.

In order to achieve the same entropy as a diceware word list we need 7776 words.  The diceware list is concerned with short character strings that are easy to remember.  I enjoy making mnemonics, and I'm not so concerned with short strings.

The English word list I used started with 97k words, and after formatting the word list a little bit and sending it through a python script I ended up with 3706 English(ish) words that alternate hands entirely. That's not enough for a diceware list, so let's start grabbing some word lists from foreign languages.

First I took the Japanese diceware list because it's the only other language I can speak.  Stripping out diceware codes was easy with awk.  `python3 alternation_check.py | wc -l` shows me that there's only 594 strings from this list, and I know a handful of them are not real Japanese words anyways (like "xj", "zy").  Removing two character words leaves me with only 276 Japanese words.  We'll add them to the list for fun but we need a lot more words.

I grabbed a handful of Russian words from the Russian National Corpus on Wiktionary.  They have 1k words per page with romanized versions of each word.  A little bit of `sed` managed to extrat the words I'm interested in from all of the other text.  I'm a little self conscious about piping sed into sed, but it was really easy.  If anybody has a better one liner I'm happy to hear it.

I manually copy and pasted 20 wiktionary pages to get a Russian word list, and I only came up with 289 words.  All that work for so little.  Это очень плохо.

Next I added German (457 words) and French diceware list (428 words).  

We're up ~5156~ 4498 word right now after removing duplicates.  

The Spanish word frequency list I found was overly comprehensive.  Total word count was 844368, yielding technically successful words like 'zuzuzuzuzuahauahauahahahahauahah'.  The Chilean-Spanish word frequency list puts us way past the needed word count by itself, it yields 17,819 alternating-hands-words.  We can use this to pad out the end of the list without using crazy words like 'zuzuzuzuzuahauahauahahahahauahah'.

20^3 is 8k, meaning if you have a twenty sided die and you roll it three times, ther are 8k different possibilities.  This isn't so far from the 5 rolls of a 6 sided die, 6^5 = 7776.  d20 are more fun, so wrote a script "generate_d20.py" that will allow you to roll 3d20 per word.

Assuming some muggles out there only have d6, I wrote another script that generates 7776 words with 5d6 codes.

note to self:  We didn't get rid of absurdly long lol strings like "hahahahahaha"
also, in the Spansish word list we have accent marks and at one point there's a cubed exponent.  Basically clean up the Spanish alternating word list and try again.

not that I'm particularly surprised, but it's in the d6_final.txt file as well.  I didn't expect this to be a problem.  qwerty uis a lot worse than I realized.  I can't even generate 8k words that alternate hands, across 6 languages?

This is hilarious, when I ran  `egrep -n "^.{$(wc -L < d20_final.txt)}$" d20_final.txt` to get longest lines, I got a bunch of lol text in Spanish (like "ajajaja").  When I deleted the thre longest offending lines (32 characters of "ajaja") the next longest lines appeared.  30 characters of "ajaja".  The problem is deeper than I realized.

Perhaps removing strings over 10 characters in length will do the trick?

```
grep -v '.\{10\}' success_words.txt > success_words_under_10_char.txt
```

16780 strings, many many iterations of laughter.  Haha, guaha, jajaj.  How do I write a rule for this?

update:  I just manually added a bunch of offensive strings into a shell script, like "ha" and "ja".  Anything you get more than 2 in a row, it skips that line.  Script is in `es/process_words.sh`, I used it to create `processed_words.txt`.

I modified `alternation_check` to remove diacritics and ran the entire Spanish word list again.  My solution was to remove all Spanish words over 5 characters long, I don't like this solution because we're missing exceptional words like "tormentoso", and I can see some pretty said gibberish at the bottom fo the list such as "wkf".  Spending any more time on Spanish section of this word list is not an effective use of my time.  Sorry to see you go, tormentoso.

### Installing

### References

* [Passwords should allow spaces](https://security.stackexchange.com/questions/32691/why-not-allow-spaces-in-a-password)
* [Remove duplicates](https://stackoverflow.com/questions/1444406/how-to-delete-duplicate-lines-in-a-file-without-sorting-it-in-unix)
* [How to get the longest line from a file](https://askubuntu.com/questions/375832/how-to-get-longest-line-from-a-file)

## Acknowledgments

* [Peter Norvig's word count file for English words](http://norvig.com/mayzner.html)
* [A G Reinhold, Diceware](http://world.std.com/~reinhold/diceware.html)
* [Word Frequency List of Chilean Spanish](http://sadowsky.cl/lifcach.html)
* [French Diceware List](https://rdrr.io/cran/riceware/man/wordlist_fr.html)
* [Russian Word Lists on Wiktionary](https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists#Russian)
* [PurpleBooth README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
