left_hand = 'QWERTASDFGZXCVB'
right_hand = 'YUIOPHJKLNM'

# spanish list is annoyingly all lower case
left_hand = 'qwertasdfgzxcvb'
right_hand = 'yuiophjklnm'

with open('es/justthefrequentwords.txt', encoding='latin1') as f:
    lines = f.readlines()
    lines = [x.strip().lower() for x in lines]
    for word in lines:
        length = len(word)
        success = True
        if length < 3:
            continue
        if word[-1] not in left_hand and word[-1] not in right_hand:
            continue
        for x in range(length - 1):
            if word[x] in left_hand:
                if word[x+1] in left_hand:
                    success = False
                    continue
            elif word[x] in right_hand:
                if word[x+1] in right_hand:
                    success = False
                    continue
            else:
                success = False
                continue


        if success == True:
            print(word)
